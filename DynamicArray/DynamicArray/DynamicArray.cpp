// DynamicArray.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>

int main()
{
	int sizeArrayX,sizeArrayY;

	std::cout << "Enter the size of dynamic array (int)\n"
	
	<< "X: ";
	std::cin >> sizeArrayX; std::cout 
	
	<< "Y: ";
	std::cin >> sizeArrayY;

	int **ptrArray = new int *[sizeArrayY];

	for(int i=0;i<sizeArrayY;i++)
	{
		ptrArray[i] = new int[sizeArrayX];

	}

	for (int i = 0; i<sizeArrayY; i++)
	{
		for (int j = 0; j<sizeArrayX; j++)
		{
			ptrArray[i][j]=rand()/22;

		}

	}

	for (int i = 0; i<sizeArrayY; i++)
	{
		for (int k = 0; k<sizeArrayX; k++)
		{
			std::cout << "[" << i << "][" << k << "] = "
				<< ptrArray[i][k] << "; ";

		}

		std::cout << "\n\n";
	}

	system("pause");

    return 0;
}

